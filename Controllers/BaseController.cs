﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CasinoModel;
using Ninject;

namespace WebCasino.Controllers
{
    public abstract class BaseController : Controller
    {  
        public SqlRepository Repository { get; set; }

        public BaseController()
        {
            Repository = new SqlRepository();
        }
    }
}
