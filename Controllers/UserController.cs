﻿using System;
using System.Linq;
using System.Web.Mvc;
using CasinoAnalysis;
using CasinoModel;
using WebCasino.Models;

namespace WebCasino.Controllers
{
    public class UserController : BaseController
    {
        public ActionResult Login(User user)
        {
            if (!string.IsNullOrWhiteSpace(user.Name))
            {
                //Repository.AddUser(user);
                //user = Repository.Users.First(u => u.Name == user.Name);
                return View(user);
            }
            else
            {
                return View((User)null);
            }

        }

        public ActionResult AddMoney(string username, int balance)
        {
            var deposite = new Deposite();
            deposite.User = new User{Name = username, Balabce = balance};
            return View(deposite);
        }

        public ActionResult ChargeMoney(Deposite deposite)
        {
            //var user = Repository.Users.FirstOrDefault(u => u.Name == deposite.User.Name);
            var user = deposite.User;
            if (deposite.Value >= 0)
            {
                deposite.Date = DateTime.Now;
                //Repository.AddDeposite(deposite);
                user.Balabce += deposite.Value;
                //Repository.UpdateUser(user);
                return View("Login", user);
            }
            else
            {
                return View("Login", user);;
            }
        }


        public ActionResult PlayDice(string username, int balance)
        {
            DicePlayer player = new DicePlayer(username, balance);
            return View(player);
        }

        public ActionResult PartyDice(DicePlayer player)
        {
            Random random = new Random();

            int dice1 = random.Next(1, 6), dice2 = random.Next(1, 6);
            player.LastGameWin = dice1 + dice2 == player.Sum;
            player.BalanceChange(((player.LastGameWin) ? +1 : -1) * player.CurrentBet);
            return View(player);
        }
    }
}