﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCasino.Models;

namespace CasinoModel
{
    class Dice
    {
        public static bool Play(CasinoPlayer player)
        {
            Console.WriteLine("---DICE GAME---");
            int bet;
            if (!Casino.MakeBet(player, out bet))
            {
                return false;
            }
            Casino.Log(Casino.MainLog, "[" + DateTimeOffset.Now.ToString() + "] " + player.Name + " " + CasinoAnalysis.GameCode.Dice.ToString() + " BET:" + bet.ToString());

            String playerSumS;
            if (!Casino.ReadAnswerDialog("Bet 2 dices sum: ", Casino.ExpectDice, out playerSumS))
            {
                return false;
            }
            int playerSum = Int32.Parse(playerSumS);

            Random random = new Random();

            int dice1 = random.Next(1, 6), dice2 = random.Next(1, 6);
            bool win;

            if (dice1 + dice2 == playerSum)
            {
                Console.WriteLine("You WON your bet:");
                player.BalanceChange(+bet);
                win = true;
            }
            else
            {
                Console.WriteLine("You LOOSE your bet:");
                player.BalanceChange(-bet);
                win = false;
            }

            Console.WriteLine("Sum: {0} = ({1} + {2})", dice1 + dice2, dice1, dice2);

            Casino.Log(Casino.MainLog,
                "[" + DateTimeOffset.Now.ToString() + "] " + player.Name + " " + CasinoAnalysis.GameCode.Dice.ToString() +
                " " + ((win) ? (CasinoAnalysis.GameResultStatus.Win.ToString() + " +") : (CasinoAnalysis.GameResultStatus.Loose.ToString()) + " -") +
                bet.ToString() + " " + playerSum.ToString() + " " + (dice1 + dice2).ToString());

            Casino.Log(Casino.DiceLog,
                string.Format("Player: {0} | bet: {1} | guess: {2} | sum: {3} = ({4} + {5}) | {6}", player.Name, bet, playerSum, dice1 + dice2, dice1, dice2, (win) ? "WIN" : "LOOSE"));

            return true;
        }
    }
}
