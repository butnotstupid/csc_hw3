﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoAnalysis;

namespace CasinoModel
{
    class CasinoAnalytic : ICasinoAnalytic
    {
        private List<Bet> BetsList;
        private List<Deposite> DepositesList;
        private List<GameResults> GameResultsList;

        public CasinoAnalytic()
        {
            BetsList = new List<Bet>();
            DepositesList = new List<Deposite>();
            GameResultsList = new List<GameResults>();
            LoadBetsDepositesGameResults();
        }

        private static String[] ParseLogString(string str)
        {
            var split = str.Split(' ');
            var args = new String[split.Length - 2];
            args[0] = split[0] + " " + split[1] + " " + split[2];
            for (int i = 3; i < split.Length; i++)
            {
                args[i - 2] = split[i];
            }
            return args;
        }

        private static Bet ParseBet(string str)
        {
            var args = ParseLogString(str);

            GameCode code;
            Enum.TryParse(args[2], out code);

            return new Bet
            {
                User = new User{Name = args[1]},
                Value = Convert.ToInt32(args[3].Substring(4)),
                GameCode = code, 
                Date = DateTime.Parse(args[0].Substring(1, args[0].Length - 2))
            };
        }

        private static Deposite ParseDeposite(string str)
        {
            var args = ParseLogString(str);

            return new Deposite
            {
                User = new User{Name = args[1]},
                Value = Convert.ToInt32(args[2].Substring(9)),
                Date = DateTime.Parse(args[0].Substring(1, args[0].Length - 2))

            };
        }

        private static GameResults ParseGameResults(string str)
        {
            var args = ParseLogString(str);
            
            GameCode code;
            Enum.TryParse(args[2], out code);

            GameResultStatus status;
            Enum.TryParse(args[3], out status);

            var date = DateTime.Parse(args[0].Substring(1, args[0].Length - 2));
            var user = new User {Name = args[1]};

            switch (code)
            {
                case GameCode.BJ:
                    return new BlackJackGameResults(
                    date, user, status, 
                    Convert.ToInt32(args[4]), Convert.ToInt32(args[5]), Convert.ToInt32(args[6]));
                    break;
                case GameCode.Roulet:
                    return new RouletGameResults(
                    date, user, status,
                    Convert.ToInt32(args[4]), args[5], Convert.ToInt32(args[6]));
                    break;
                case GameCode.Dice:
                    return new DiceGameResults(
                    date, user, status, 
                    Convert.ToInt32(args[4]), Convert.ToInt32(args[5]), Convert.ToInt32(args[6]));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            
        }

        public void LoadBetsDepositesGameResults()
        {
            using (StreamReader streamReader = new StreamReader(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + Casino.MainLog))
            {
                while (!streamReader.EndOfStream)
                {
                    var str = streamReader.ReadLine();
                    if (str != null)
                    {
                        if (str.Contains("BET")) BetsList.Add(ParseBet(str));
                        else if (str.Contains("DEPOSITE")) DepositesList.Add(ParseDeposite(str));
                        else GameResultsList.Add(ParseGameResults(str));
                    }
                }
            }
        }

        public IEnumerable<Bet> Bets
        {
            get { return BetsList; }
        }

        public IEnumerable<Deposite> Deposites
        {
            get { return DepositesList; }
        }

        public IEnumerable<GameResults> GamesResults
        {
            get { return GameResultsList; }
        }

        public Bet MaxBet()
        {
            return Bets.OrderByDescending(bet => bet.Value).First();
        }

        public Bet MaxBet(GameCode code)
        {
            return Bets.Where(bet => bet.GameCode == code).OrderByDescending(bet => bet.Value).First();
        }

        public int AverageDeposite()
        {
            return Deposites.Sum(dep => dep.Value) / Deposites.Count();
        }

        public int AverageDeposite(User user)
        {
            return Deposites.Where(dep => dep.User.Name == user.Name).Sum(dep => dep.Value) / Deposites.Count(dep => dep.User.Name == user.Name);
        }

        public IEnumerable<Deposite> TopDeposites(int count)
        {
            return Deposites.GroupBy(dep => dep.User.Name).Select(group => group.OrderByDescending(dep => dep.Value).First()).Take(count);
        }

        public IEnumerable<Deposite> RichestClients(int count)
        {
            return Deposites.OrderByDescending(dep => Deposites
                .Where(depuser => depuser.User.Name == dep.User.Name)
                .Sum(depuser => depuser.Value))
                .Take(count);
        }

        public GameCode MaxProfitGame(out int amount)
        {
            amount = GamesResults.GroupBy(gr => gr.GameCode).Min(group => group.Sum(res => res.BalanceChange)) * (-1);
            return
                GamesResults.GroupBy(gr => gr.GameCode)
                    .OrderBy(group => group.Sum(res => res.BalanceChange))
                    .First().Key;
        }

        public User MostLuckyUser(GameCode game)
        {
            return 
                GamesResults.Where(gr => gr.GameCode == game)
                .GroupBy(gr => gr.User)
                .OrderByDescending(group => (1 + group.Count(res => res.Status == GameResultStatus.Win)) / (1 + group.Count(res => res.Status == GameResultStatus.Loose)))
                .First().Key;
        }

        public User MaxLuckyUser()
        {
            return GamesResults.GroupBy(gr => gr.User)
                .OrderByDescending(group => (1 + group.Count(res => res.Status == GameResultStatus.Win)) / (1 + group.Count(res => res.Status.Equals(GameResultStatus.Loose))))
                .First().Key;
        }

        public int UserDeposite(User user, DateTime date)
        {
            return Deposites.Where(dep => dep.User.Name == user.Name && dep.Date == date).Sum(dep => dep.Value);
        }

        public IEnumerable<int> ZeroBasedBalanceHistoryExchange(User user)
        {
            var prefix = 0;
            return GamesResults.Where(gr => gr.User.Name == user.Name)
                .Select(gr => prefix += gr.BalanceChange)
                .ToList();
            
        }

        public IEnumerable<int> ZeroBasedBalanceHistoryExchange(User user, DateTime @from)
        {
            var prefix = 0;
            return GamesResults.Where(gr => gr.User.Name == user.Name && gr.Date >= from)
                .Select(gr => prefix += gr.BalanceChange)
                .ToList();
        }

        public Dictionary<DateTime, int> ProfitByMonths()
        {
            return 
                GamesResults.GroupBy(gr => gr.Date.Month)
                .ToDictionary(group => new DateTime(group.First().Date.Month), group => group.Sum(gr => gr.BalanceChange) * (-1));
        }

        public Dictionary<DateTime, int> GamesCountByMounths()
        {
            return
                GamesResults.GroupBy(gr => gr.Date.Month)
                .ToDictionary(group => new DateTime(group.First().Date.Month), group => group.Count());
        }

        public Dictionary<DateTime, int> GamesCountByMounths(GameCode game)
        {
            return
                GamesResults.Where(gr => gr.GameCode == game).GroupBy(gr => gr.Date.Month)
                .ToDictionary(group => new DateTime(group.First().Date.Month), group => group.Count());
        }

        public Dictionary<DateTime, int> NewUsersByMounths()
        {
            return
                GamesResults.GroupBy(gr => gr.User.Name)
                    .Select(group => group.OrderBy(gr => gr.Date).First())
                    .GroupBy(gr => gr.Date.Month)
                    .ToDictionary(group => new DateTime(group.First().Date.Month), group => group.Count());
        }

        public Dictionary<DateTime, int> NewUsersByMounths(GameCode game)
        {
            return
                GamesResults.Where(gr => gr.GameCode == game)
                    .GroupBy(gr => gr.User.Name)
                    .Select(group => group.OrderBy(gr => gr.Date).First())
                    .GroupBy(gr => gr.Date.Month)
                    .ToDictionary(group => new DateTime(group.First().Date.Month), group => group.Count());
        }
    }
}
