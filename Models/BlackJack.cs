﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using WebCasino.Models;

namespace CasinoModel
{
    class BlackJack
    {
        public static string CardsToString(int[] playerCards, int playerCardsCount)
        {
            String buf = "";
            for (int i = 0; i < playerCardsCount - 1; i++)
            {
                buf += string.Format("{0}, ", playerCards[i]);
            }
            buf += string.Format("{0}",playerCards[playerCardsCount - 1]);
            return buf;
        }

        public static void ShowCards(int[] playerCards, int playerCardsCount, int[] casinoCards, int casinoCardsCount)
        {
            Console.WriteLine("Your cards are: ");
            Console.WriteLine(CardsToString(playerCards, playerCardsCount));
            Console.WriteLine("Casino cards are: ");
            Console.WriteLine(CardsToString(casinoCards, casinoCardsCount));
        }

        public static bool Play(CasinoPlayer player)
        {
            Console.WriteLine("---BLACK JACK GAME---");
            int bet;
            if (!Casino.MakeBet(player, out bet))
            {
                return false;
            }
            Casino.Log(Casino.MainLog, "[" + DateTimeOffset.Now.ToString() + "] " + player.Name + " " + CasinoAnalysis.GameCode.BJ.ToString() + " BET:" + bet.ToString());

            Random random = new Random();

            int[] playerCards = new int[11];
            int playerCardsCount = 2;
            playerCards[0] = random.Next(2, 11);
            playerCards[1] = random.Next(2, 11);
            int playerSum = playerCards[0] + playerCards[1];

            Console.WriteLine("Your cards are: ");
            Console.WriteLine(CardsToString(playerCards, playerCardsCount));

            while (playerSum < 22)
            {
                String yn;
                if (!Casino.ReadAnswerDialog("One more card?: ", Casino.ExpectYesNo, out yn)) return false;
                if (yn.Equals("y"))
                {
                    playerSum += playerCards[playerCardsCount++] = random.Next(2, 11);
                    Console.WriteLine("Your cards are: ");
                    Console.WriteLine(CardsToString(playerCards, playerCardsCount));
                }
                else break;
            }

            int[] casinoCards = new int[11];
            int casinoCardsCount = 2;
            casinoCards[0] = random.Next(2, 11);
            casinoCards[1] = random.Next(2, 11);
            int casinoSum = casinoCards[0] + casinoCards[1];

            while (casinoSum < 16) casinoSum += casinoCards[casinoCardsCount++] = random.Next(2, 11);

            bool win;

            if (playerSum > 21 || (playerSum <= casinoSum && casinoSum <= 21))
            {
                Console.WriteLine("You LOOSE your bet:");
                ShowCards(playerCards, playerCardsCount, casinoCards, casinoCardsCount);
                player.BalanceChange(-bet);
                win = false;
            }
            else
            {
                Console.WriteLine("You WON your bet:");
                ShowCards(playerCards, playerCardsCount, casinoCards, casinoCardsCount);
                player.BalanceChange(+bet);
                win = true;
            }

            Casino.Log(Casino.MainLog, 
                "[" + DateTimeOffset.Now.ToString() + "] " + player.Name + " " + CasinoAnalysis.GameCode.BJ.ToString() +
                " " + ((win) ? (CasinoAnalysis.GameResultStatus.Win.ToString() + " +") : (CasinoAnalysis.GameResultStatus.Loose.ToString()) + " -") + 
                bet.ToString() + " " + playerSum.ToString() + " " + casinoSum);

            Casino.Log(Casino.BlackJackLog,
                string.Format("Player: {0} | bet: {1} | cards: ", player.Name, bet) +
                CardsToString(playerCards, playerCardsCount) + 
                " | casino cards: " + 
                CardsToString(casinoCards, casinoCardsCount) + " | " + ((win) ? "WIN" : "LOOSE"));

            return true;
        }  
    }
}
