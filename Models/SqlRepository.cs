﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoAnalysis;
using Ninject;

namespace CasinoModel
{
    public class SqlRepository : IRepository
    {
        public SqlRepository()
        {
            Db = new CasinoDBContext();
        }

        public CasinoDBContext Db { get; set; }

        public IQueryable<User> Users
        {
            get { return Db.Users; }
        }

        public IQueryable<Bet> Bets
        {
            get { return Db.Bets; }
        }

        public IQueryable<GameResults> GameResults
        {
            get { return Db.GameResults; }
        }

        public IQueryable<Deposite> Deposites
        {
            get { return Db.Deposites; }
        }

        public bool AddUser(User user)
        {
            bool res = false;
            if (Db.Users.FirstOrDefault(u => u.Name == user.Name) == null)
            {
                res = true;
                Db.Users.Add(user);
                Db.SaveChanges();
            }
            return res;
        }

        public bool UpdateUser(User user)
        {
            bool res = false;
            User foundUser = Db.Users.FirstOrDefault(u => u.Name == user.Name);
            
            if (foundUser != null)
            {
                foundUser.Balabce = user.Balabce;
                Db.SaveChanges();
                res = true;
            }

            return res;
        }

        public bool DeleteUser(string username)
        {
            bool res = false;
            User foundUser = Db.Users.FirstOrDefault(u => u.Name == username);

            if (foundUser != null)
            {
                Db.Users.Remove(foundUser);
                Db.SaveChanges();
                res = true;
            }

            return res;
        }

        public bool AddBet(Bet bet)
        {
            bool res = false;
            if (bet.Id == 0)
            {
                Db.Bets.Add(bet);
                Db.SaveChanges();
                res = true;
            }
            return res;
        }


        public bool DeleteBet(int betId)
        {
            Bet foundBet = Db.Bets.FirstOrDefault(p => p.Id == betId);
            if (foundBet != null)
            {
                Db.Bets.Remove(foundBet);
                Db.SaveChanges();
                return true;
            }

            return false;
        }

        public bool AddDeposite(Deposite deposite)
        {
            bool res = false;
            if (deposite.Id == 0)
            {
                Db.Deposites.Add(deposite);
                Db.SaveChanges();
                res = true;
            }
            return res;
        }

        public bool DeleteDeposite(int id)
        {
            Deposite doundDeposite = Db.Deposites.FirstOrDefault(p => p.Id == id);
            if (doundDeposite != null)
            {
                Db.Deposites.Remove(doundDeposite);
                Db.SaveChanges();
                return true;
            }

            return false;
        }

        public bool AddGameResult(GameResults gameResult)
        {
            bool res = false;
            if (gameResult.Id == 0)
            {
                Db.GameResults.Add(gameResult);
                Db.SaveChanges();
                res = true;
            }
            return res;
        }

        public bool DeleteGameResult(int gameResultId)
        {
            GameResults foundGameResult = Db.GameResults.FirstOrDefault(p => p.Id == gameResultId);
            if (foundGameResult != null)
            {
                Db.GameResults.Remove(foundGameResult);
                Db.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
